import { Vehicle } from '../lib/model'
import { VehicleRepository } from '../lib/dynamo'
import { test, todo } from 'tap'

test('Persisting a vehicle', async ({ same }) => {
  let vehicleId: string
  const input = new Vehicle()
  const repo = new VehicleRepository()

  repo.add(input)
  await repo.flush()

  const output = await repo.get(vehicleId)

  same(input, output)
})
