import { Vehicle } from '../lib/model'
import { test, todo } from 'tap'

test('On the day of purchase', async () => {
  const vehicle = new Vehicle()

  vehicle.calculatePrice()

  test('the current price should equal the forecourt price', async ({ is }) => {
    is(vehicle.currentPrice, vehicle.forecourtPrice)
  })

  test('does not raise any events', async ({ same }) => {
    same(vehicle.events, [])
  })
})

todo('When the calculated price is ugly', () => {
  test('the current price should quantise to the nearest marketing-friendly price', () => {})
  test('raises the vehicle-price-recalculated event')
})

todo('On the sell by date', () => {
  test('the current price should equal the sell by price', () => {})

  test('raises the vehicle-price-recalculated event')
})

todo('At the mid point', () => {
  test('the current price should be halfway between the forecourt price and sell by price', () => {})
})

todo('When the sell by date has expired', () => {
  test('the current price should depreciate twice as quickly', () => {})
})

todo('When we apply a manual price adjustment', () => {
  test('the current price is equal to the adjusted price', () => {})

  test('raises the vehicle-price-recalculated-event', () => {})
})

todo('When recalculating an adjusted price', () => {
})
