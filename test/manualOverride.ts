import { test } from 'tap'
import { VehicleRepository, EventRaiser } from '../lib/ports'
import { Vehicle } from '../lib/model'
import * as services from '../lib/services'

test('When manually overriding a price', async () => {
  let repository: VehicleRepository
  let events: EventRaiser
  const vehicle = new Vehicle()

  repository.add(vehicle)

  await services.overridePrice(repository, events, 'vehicle-id', 15000)

  test('the vehicle current price should be updated', async ({ is }) => {
    is(vehicle.currentPrice, 15000)
  })

  test('the vehicle-price-recalculated event should have been raised', async () => {
  })

  test('the repository changes should have been flushed');
})
