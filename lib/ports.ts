import { Vehicle } from './model'

export interface VehicleRepository {
  get(vehicleId: string): Vehicle
  add(vehicle: Vehicle): void
  flush(): void
}

export interface EventRaiser {
  publish(name: string, payload: object): void
}
