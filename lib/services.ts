import { VehicleRepository, EventRaiser } from './ports'

export function overridePrice (
  repository: VehicleRepository,
  events: EventRaiser,
  vehicleId: string,
  newPrice: number
): void {
  const vehicle = repository.get(vehicleId)
  console.log(newPrice)

  repository.flush()
  vehicle.events.forEach(e => events.publish('vehicle-price-recalculated', e))
}
