import * as dynamo from 'dynamodb'
import { Vehicle } from './model'

const Vehicles = dynamo.define('Vehicle', {
  hashKey: 'id',

  timestamps: true,

  schema: {
  }
})

export class DynamoDbRepository {
  async get (vehicleId: string) {
    const data = await Vehicles.get(vehicleId).promise()
    console.log(data)
    return new Vehicle()
  }
}
